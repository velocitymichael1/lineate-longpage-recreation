import jQuery from 'jquery'
import AOS from 'aos';

window.$ = window.jQuery = jQuery

AOS.init();

$( document ).ready( () => {
    console.log( 'Document ready!' )
})



const hamburger = document.getElementById('hamburger');
const mobileNav = document.getElementById('mobile-nav');
const bar1 = document.getElementById('bar1')
const bar2 = document.getElementById('bar2')
const bar3 = document.getElementById('bar3')

hamburger.addEventListener('click', () => {
    mobileNav.classList.toggle('show')
    bar1.classList.toggle('change');
    bar2.classList.toggle('change');
    bar3.classList.toggle('change');

})
