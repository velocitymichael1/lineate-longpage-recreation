<?php
function problem_header($problem_header_data){
    ob_start();
    ?>
    <section data-aos="zoom-out-up" data-aos-duration="1200" class="blob-illustration-wrapper mt-5 mt-md-14 mt-lg-18">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 d-flex flex-column justify-content-center align-items-center blob-background-container <?php echo $problem_header_data['image_class']; ?>">
                    <p><?php echo $problem_header_data['sub_title'];?></p>
                    <h2><?php echo $problem_header_data['title'];?></h2>
                </div>
            </div>      
        </div>
    </section>
    <?php

    $buffer = ob_get_contents();

    ob_end_clean();

    return $buffer;
}

?>

