<?php 

$blue_container_classes = array(
    'blue-infographic-container' => 'blue-infographic-container p-2 p-sm-4 p-md-6 p-lg-10',
    'left-aligned-row' => 'infographic-row d-flex flex-row mb-3 justify-content-start align-items-center row',
    'right-aligned-row' => 'infographic-row d-flex flex-row mb-3 justify-content-end align-items-center row',
    'p-max-width' => 'max-width-text',
    'image-holder-left' => 'd-flex justify-content-center align-items-center icon-image-holder image-holder-left mb-4 pt-md-4 pb-md-4',
    'image-holder-right' => 'd-flex justify-content-center align-items-center icon-image-holder image-holder-right mb-4 pt-md-4 pb-md-4',
    'image-column-left' => 'col-12 col-md-4 col-lg-3 col-xl-3 image-left-column',
    'image-column-right' =>  'col-12 col-md-4  col-lg-3 col-xl-3 order-sm-1 order-md-2',
    'text-column-left' => 'col-12 col-md-6  col-lg-6 col-xl-6 col-offset-4 order-2 order-md-1',
    'text-column-right' => 'col-12 col-md-6 col-lg-6 col-xl-6 offset-lg-0',
    'infographic-button-div' => 'infographic-button d-flex justify-content-center align-items-center'
);

?>