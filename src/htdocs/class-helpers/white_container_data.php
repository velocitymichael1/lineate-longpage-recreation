<?php 

    $white_container_classes = array(
        'white-infographic-wrapper' => 'col-10 p-2 p-sm-4 p-md-6  p-lg-8  white-infographic',
        'row' => 'row d-flex justify-content-center align-items-center',
        'image-column' => 'col-12 col-md-12 col-lg-6  mb-4 image-column',
        'text-column' => 'col-12 col-md-12 col-lg-6  mb-2 text-column',
    );

?>