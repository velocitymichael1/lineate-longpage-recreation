<section  class="text-hero-wrapper mt-12 mt-xl-25">
    <article class="container">
        <div class="row text-hero-container">
            <div class="col-12 col-md-12 col-lg-4 col-xl-4 heading-container">
                <h3>Outsourcing is typically a headache.</h3>
            </div>
            <div class="col-12 col-md-12 col-lg-8 col-xl-8 mt-2">
                <p>
                    That might sound a bit odd coming from a company that specializes in outsourced services - but hey, we’ve got to reckon with the facts.
                </p>
                <p>
                    Maybe it’s more accurate to say that if mishandled, outsourcing can be a source of profound frustration, distended project timelines and ill-spent resources. 
                </p>  
                <p>  
                    Of course, recognizing the common reasons why outsourced projects fail is the first step towards building ones that don’t. 
                </p>
                <p>
                    So, even from the earliest days of Lineate, we knew we’d have to apply our obsession for problem-solving to the issues inherent in outsourcing. So, we did. And it worked.
                </p>  
                </p>                  
                    Let’s dive into the three major problems companies tend to run into when outsourcing work (and we’ll spill on how we solved them.) 
                </p>

            </div>
        </div>
    </article>
</section>

