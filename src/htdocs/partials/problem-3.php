<?php
    $problem_header_data = array(
        'blob' => 'blob_3',
        'sub_title' => 'Major Problem #3',
        'title' => 'The knowledge-in-practice gap.',
        'image_class' => 'blob-background-image-3'
    );

    echo problem_header($problem_header_data);

?>

<section>
        <div class="container">
            <div class="<?php echo $white_container_classes['row']; ?>">
                <article class="<?php echo $white_container_classes['white-infographic-wrapper']; ?>">
                    <h4 class="mb-6">Expertise isn’t as standardized in actuality as it is on a skills sheet. Having technical know-how is one thing - but being able to actually apply that knowledge across a range of varying contexts is another.</h4>
                    <div class=" d-flex row">
                        <div class="<?php echo $white_container_classes['text-column']; ?>">
                            <p class="mb-2">So, what happens when the actual skills of your outsourced team don’t match up to the requirements of the project?</p>
                            <p class="mb-2">What if they’re prepared to offer boilerplate solutions for a project that requires custom architecture, custom data pipelining, custom thinking?</p>
                            <p class="mb-2">What if the needs of the project grow beyond the outsourced team’s technical capabilities?</p>
                            <p class="mb-2">And what if your time spent hand-holding is more trouble than it’s worth?</p>
                        </div>
                        <div class="<?php echo $white_container_classes['image-column']; ?>">
                            <img src="images/GettyImages-1180183363-GRADED 1.jpg" class="border-colour-blue">
                        </div>
                    </div>
                    <p class="bottom-p mt-2"><strong>Again, this is where the soft skills and intangibles like a capacity for problem-solving add up. Companies can find out the hard way that technical expertise isn’t always what it says on the tin. </strong></p>
                </article>

                <?php //Blue Paper ?>
                <article class="col-12">
                    <div class="blue-infographic">
                        <div class="<?php echo $blue_container_classes['blue-infographic-container']; ?>">
                            <h1 class="mb-3 mb-md-5">How we cracked it:</h1>

                            <?php //Left Aligned Row ?>
                            <div data-aos="fade-right" data-aos-duration="1200" class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                                <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                    <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                        <img src="images/outline-lineate-icon-difficult.svg"/>
                                    </div>
                                </div>
                                <div class="<?php echo $blue_container_classes['text-column-right']; ?>">
                                    <h4 class="mb-2">We live for the most difficult work.</h4>
                                    <p>Head-banging, ultra-complex software problems - they’re kind of our thing. That means not only applying our team’s world-class technical expertise, but knowing how and where it’s best applied.</p>
                                </div>
                            </div>

                            <?php //Right Aligned Row ?>
                            <div data-aos="fade-left" data-aos-duration="1200"   class="<?php echo $blue_container_classes['right-aligned-row']; ?>">
                                <div class="<?php echo $blue_container_classes['text-column-left']; ?>">
                                    <div class="flex-column mb-4 <?php echo $blue_container_classes['p-max-width']; ?>">
                                        <h4 class="mb-2">Custom means custom.</h4>
                                        <p>Our Lockstep™ method means we go DEEP into the details of your project. You won’t find a trace of one-size-fits-all in our methodology.</p>
                                        <p>The time we spend with you in discovery accelerates everything. More than just brainstorming the technical details - we immerse ourselves in how your business works. This lets us quickly go from a high-level proposal to a superbly precise plan.</p>
                                        <p>Oh, and we don’t consider a project a success unless it’s truly future-proof. High standards? You betcha.</p>
                                    </div>
                                </div>
                                <div  class="<?php echo $blue_container_classes['image-column-right']; ?>">
                                    <div class="<?php echo $blue_container_classes['image-holder-right']; ?> ">
                                        <img src="images/outline-lineate-icon-custom.svg"/>
                                    </div>
                                </div>
                            </div>

                            <?php //Left Aligned Row ?>
                            <div data-aos="fade-right" data-aos-duration="1200"  class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                                <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                    <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                        <img src="images/outline-lineate-icon-knowledge.svg"/>
                                    </div>
                                </div>
                                <div class="<?php echo $blue_container_classes['text-column-right']; ?> mb-2">
                                    <div class="flex-column <?php echo $blue_container_classes['p-max-width']; ?>">
                                        <h4 class="mb-2">We leave you more knowledgable.</h4>
                                        <p>You’ve heard the old axiom: good code is documented out. We take that a few dozen steps further. We’re not just talking a few comments - we keep our processes recorded, tasks documented, plan changes marked out.</p>
                                        <p>Simply put: we want your internal team to understand what we’ve built once we’ve left. Building out from what we’ve crafted should be easy. Even if we’re not the ones tasked with future work - we care that whoever undertakes it has a clear trail to follow.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
</section>