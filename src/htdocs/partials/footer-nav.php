<footer class="container mt-6 mt-md-10 pb-4">
    <div class="footer-container row p-3">
        <div class="logo-container d-flex align-items-center justify-content-center justify-content-lg-start col-12 col-lg-6" >
            <img src="/images/lineate_reverse_medium 1.png"/>
        </div>
        <div class="nav-container d-flex align-items-center col-12 col-lg-6 ">
            <nav class="navigation d-flex flex-column flex-lg-row justify-content-between align-items-center">
                <a href="#">Who we are</a>
                <a href="#">What we do</a>
                <a href="#">Our work</a>
                <a href="#">Our community</a>
            <nav>
        </div>
    </div> 
    <div class="row">
        <div class="col-6 d-xs-none d-sm-none d-md-flex">
        </div>
        <div class="copyright-text-wrapper justify-content-end col-12 col-lg-6">
            <div class="copyright-text-div d-flex flex-column  justify-content-center align-items-center justify-content-lg-end align-lg-items-end">
                <span>
                    © Copyright 2021 Lineate LLC
                </span>
                <span>
                    Data Privacy Policy
                </span>
            </div>
        </div>
    </div>
</footer>
