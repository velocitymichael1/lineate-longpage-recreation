<section data-aos="fade-up" data-aos-duration="3000" style="overflow: none" class="hero-wrapper">
    <div class="row justify-content-end">
        <div class="hero-image-container d-flex justify-content-center  col-12 d-xl-inline col-xl-6">
            <img width="900" height="671" src="/images/hero-image.jpg " class="hero-image img-fluid" alt="hero-image"/>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="d-flex hero-container py-5 py-lg-10 ">
                <div class="col-12 col-md-12 col-lg-12 col-xl-7 pt-3 heading-container">
                    <h1>The 3 Major Problems With Outsourcing</h1>
                    <h4>(And How We Fixed Them)</h4>
                </div>
            </div>
        </div>
    </div>
</section>



