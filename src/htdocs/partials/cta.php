<section>
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-8 bottom-block-text">
                <h4>If you’re outsourcing, it’s probably for a good reason.</h4>
                <p class="p-max">Maybe the scope of the project exceeds the bandwidth of your personnel. Maybe it requires a specific technical knowledge and experience set. Or maybe the project is just really f***ing hard.  </p>
                <p>Either way, outsourcing is the right move for certain projects. But you need a partner that not only recognizes the pitfalls of outsourcing, but builds across them.</p>
                <div class="infographic-button d-flex justify-content-center align-items-center">
                    <a href="#">Get in touch now</a>
                </div>
            </div>
        </div>
    </div>
</section>
