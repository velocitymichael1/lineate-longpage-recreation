<?php
$problem_header_data = array(
    'blob' => 'blob_2',
    'sub_title' => 'Major Problem #2',
    'title' => 'Slowdowns',
    'image_class' => 'blob-background-image-2'
);

echo problem_header($problem_header_data);
?>
<section>
    <div class="container">
        <?php //White Paper ?>
        <div class="<?php echo $white_container_classes['row']; ?>">
            <article class="<?php echo $white_container_classes['white-infographic-wrapper']; ?>">
                <p class="mb-2">The decentralized nature of outsourcing means that projects are often faced with speed bumps - issues that bog down the pace of collaborative work.</p>
                <p class="mb-2">Granted, as the world continues a hefty pivot towards remote work, we’re all kinda getting better at it. However - there are levels to the remote collaboration game.</p>
                <h4 class="mt-4 mb-6">The best outsourcers aren't just the most knowledgeable or talented - they're masters of performing from afar. Isolated excellence loses momentum across borders. If that fact is ignored, your project won’t exactly be flitting along at a smooth pace.</h4>
                <div class="d-flex row">
                    <div class="<?php echo $white_container_classes['image-column']; ?>">
                        <img src="images/GettyImages-607477471-GRADED 1.jpg" class="border-colour-green">
                    </div>
                    <div class="<?php echo $white_container_classes['text-column']; ?> mt-xl-4">
                        <p>Say, for example - if your outsourced and in-house teams are coordinating across time zones. Your people run into a high-impact blocker at 4PM local time, but your outsourced team is off the clock. If neither team is prepared for asynchronous problem-solving, it can devolve into a staggered back-and-forth effort that weighs down the pace of your project and puts deadline stress on everyone involved.</p>
                    </div>
                </div>
                <p class="bottom-p mt-2"><strong>Or sometimes, there’s a change in business need that requires a deft pivot on an aspect of the project. Communicating that initiative across asynchronous teams can eat away at time. Taking the relevant issues and distilling them into cold, hard tasks that the outsourced devs can work with could almost be a job unto itself... *cue foreshadowing*</strong></p>
            </article>

            <?php //Bllue Paper ?>
            <article class="col-12">
                <div class="blue-infographic">
                    <div class="<?php echo $blue_container_classes['blue-infographic-container']; ?>">
                        <h1 class="mb-3 mb-md-5">How we cracked it:</h1>

                        <?php //Left Aligned Row ?>
                        <div data-aos="fade-right" data-aos-duration="1200" class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                    <img src="images/outline-lineate-icon-flexibility.svg"/>
                                </div>
                            </div>
                            <div class="<?php echo $blue_container_classes['text-column-right']; ?>">
                                <h4 class="mb-2">A focus on flexibility.</h4>
                                <p>We hire for curiosity. We hire for self-awareness. We hire for a love of collaborative problem-solving.</p>
                            </div>
                        </div>

                        <?php //Right Aligned Row ?>
                        <div data-aos="fade-left" data-aos-duration="1200"   class="<?php echo $blue_container_classes['right-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['text-column-left']; ?>">
                                <div class="flex-column mb-4">
                                    <h4 class="mb-2">We develop with soft skills in mind.</h4>
                                    <p class="<?php echo $blue_container_classes['p-max-width']; ?>">From Day 1 of onboarding a new hire, we’re developing their soft skill set. We know that soft skills are a crucial ingredient for skilled specialists performing well. You simply can't isolate expertise from empathy in outsourcing - you need the hard stuff (deep domain knowledge) and the soft stuff (being a cultural chameleon).</p>
                                </div>
                            </div>
                            <div  class="<?php echo $blue_paper_classes['image-column-right']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-right']; ?> ">
                                    <img src="images/outline-lineate-icon-dev.svg"/>
                                </div>
                            </div>
                        </div>

                        <?php //Left Aligned Row ?>
                        <div data-aos="fade-right" data-aos-duration="1200"  class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                    <img src="images/outline-lineate-icon-refining.svg"/>
                                </div>
                            </div>
                            <div class="<?php echo $blue_container_classes['text-column-right']; ?> mb-2">
                                <div class="flex-column <?php echo $blue_container_classes['p-max-width']; ?>">
                                    <h4 class="mb-2">Years of refining our process..</h4>
                                    <p>We know what works in terms of remote collaboration because we’ve experienced, first-hand and painfully, what doesn’t.</p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <h3>Think we can help you tackle similar challenges?</h3>
                        <div class="<?php echo $blue_container_classes['infographic-button-div']; ?>">
                            <a href="#">See our work here</a>
                        </div>
                    </div>
                </div>
            </article>

        </div>
    </div>
</section>