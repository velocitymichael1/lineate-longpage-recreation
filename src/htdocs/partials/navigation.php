<div id="mobile-nav" class="mobile-toggle-menu mobile-toggle flex-column justify-content-center align-items-center">
    <a>Who we are</a>
    <hr/>
    <a>What we do</a>
    <hr/>
    <a>Our work</a>
    <hr/>
    <a>Our community</a>
    <hr/>
</div>
<header>
    <div class="container">
        <div class="header-container row p-1 p-md-3">
            <div class="logo-container d-flex align-items-center col-6 col-lg-4 col-xl-6" >
                <img src="images/lineate_reverse_medium 1.png" class="img-fluid"/>
            </div>
            <div class="nav-container d-flex align-items-center  col-6 col-lg-8 col-xl-6">
                <nav class="navigation ">
                    <div class="desktop-menu justify-content-between align-items-center d-none d-lg-flex">
                        <a href="#">Who we are</a>
                        <a href="#">What we do</a>
                        <a href="#">Our work</a>
                        <a href="#">Our community</a>
                        <img  src="images/icon-search.svg" alt="magnifying-glass"/>
                    </div>
                    <div class="mobile-nav-menu d-flex flex-column justify-content-end align-items-end col-6 d-lg-none img-fluid" id="hamburger">
                            <span class="bar-1" id="bar1"></span>
                            <span class="bar-2" id="bar2"></span>
                            <span class="bar-3" id="bar3"></span>
                    </div>
                <nav>
            </div>
        </div> 
    </div>     
</header>
<?php
