<?php
$problem_header_data = array(
    'sub_title' => 'Major Problem #1',
    'title' => 'Overlooking soft Factors.',
    'image_class' => 'blob-background-image-1'
);

echo problem_header($problem_header_data);
?>
<section>
    <div class="container">
        <div class="<?php echo $white_container_classes['row']; ?> ">
            <?php //White Paper ?>

            <article class="<?php echo $white_container_classes['white-infographic-wrapper']; ?>">
                <p class="mb-2">Soft skills aren’t just a nice-to-have - they’re integral to the success of any project. </p>
                <p class="mb-2">Now, most companies recognize this when it comes to hiring in-house talent.</p>
                <p class="mb-2">Case in point: <span>92% of hiring professionals reported that soft skills are equally or more important to hire for than hard skills.</span> What’s more, 89% said that when a new hire doesn’t work out, it’s because they lack critical soft skills.</p>
                <h4 class="mt-4 mb-6">The thing is: when it comes to outsourcing for a project, soft skills tend to take a firm backseat to technical skills.</h4>
                <div class="row d-flex">
                    <div class="<?php echo $white_container_classes['text-column']; ?> mt-4">
                        <p class="pb-2">And that’s somewhat understandable, right? After all, soft skills are harder to assess than technical skills. Plus, when you’re looking to outsource work to technical specialists, you aren't hiring a peer for life - it’s often short-term. So a lack of soft skills can feel like an acceptable place to lower the bar.</p>
                        <p class="pb-2">BUT that’s a false economy. When you’re trusting an outsider to deliver critical, specialized work (that you don't necessarily have the expertise to manage or measure), that’s when the importance of soft skills rises, not falls.</p>
                    </div>
                    <div class="<?php echo $white_container_classes['image-column'];?> mt-lg-6 mt-xl-0">
                        <img src="images/whitepaper-image-1.jpg" class="border-colour-red">
                    </div>      
                </div>
                <p class="bottom-p mt-2"><strong>If your outsourced team doesn’t have the emotional intelligence, communication skills, good judgement and practical creativity that it takes to deliver an outstanding project while working alongside your in-house team? The options are to push through the dysfunction, scrap the project, or roll the dice on another company.</strong></p>
            </article>

            <?php //Blue Paper ?>

            <article class="col-12">
                <div class="blue-infographic">
                    <div class="<?php echo $blue_container_classes['blue-infographic-container']; ?>">
                        <h1 class="mb-3 mb-md-5">How we cracked it:</h1>

                        <?php //Left Aligned Row ?>
                        <div data-aos="fade-right" data-aos-duration="1200" class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                    <img src="images/outline-lineate-icon-hiring.svg"/>
                                </div>
                            </div>
                            <div class="<?php echo $blue_container_classes['text-column-right']; ?>">
                                <h4 class="mb-2">We hire with soft skills in mind</h4>
                                <p>We hire for curiosity. We hire for self-awareness. We hire for a love of collaborative problem-solving.</p>
                            </div>
                        </div>

                        <?php //Right Aligned Row ?>
                        <div data-aos="fade-left" data-aos-duration="1200"   class="<?php echo $blue_container_classes['right-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['text-column-left']; ?>">
                                <div class="flex-column mb-4">
                                    <h4 class="mb-2">We develop with soft skills in mind.</h4>
                                    <p class="<?php echo $blue_container_classes['p-max-width']; ?>">From Day 1 of onboarding a new hire, we’re developing their soft skill set. We know that soft skills are a crucial ingredient for skilled specialists performing well. You simply can't isolate expertise from empathy in outsourcing - you need the hard stuff (deep domain knowledge) and the soft stuff (being a cultural chameleon).</p>
                                </div>
                            </div>
                            <div  class="<?php echo $blue_container_classes['image-column-right']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-right']; ?> ">
                                    <img src="images/outline-lineate-icon-dev.svg"/>
                                </div>
                            </div>
                        </div>

                        <?php //Left Aligned Row ?>
                        <div data-aos="fade-right" data-aos-duration="1200"  class="<?php echo $blue_container_classes['left-aligned-row']; ?>">
                            <div class="<?php echo $blue_container_classes['image-column-left']; ?>">
                                <div class="<?php echo $blue_container_classes['image-holder-left']; ?>">
                                    <img src="images/outline-lineate-icon-team.svg"/>
                                </div>
                            </div>
                            <div class="<?php echo $blue_container_classes['text-column-right']; ?> mb-2">
                                <div class="flex-column <?php echo $blue_container_classes['p-max-width']; ?>">
                                    <h4 class="mb-2">We know your team are ultimately the ones to impress.</h4>
                                    <p>Sometimes, when we’re planning out a project, the client will voice reservations about how their internal team will respond to working with a bunch of strangers.</p>
                                    <p>Honestly? Our customers’ in-house teams are often our loudest champions. Our people help them produce excellence in areas where they may not be trained. We take on complex tasks they don’t have time to tackle. It’s not uncommon to see the two teams form a genuine bond.</p>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <h3>Want to see examples of how we solved this for others?</h3>
                        <div class="<?php echo $blue_container_classes['infographic-button-div']; ?>">
                            <a href="#">See our work here</a>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>
  