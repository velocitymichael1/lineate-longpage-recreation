<?php
//Imports for foreach to loop through and put data into each blob section
include 'class-helpers/white_container_data.php';
include 'class-helpers/blue_container_data.php';
include 'function-helpers/problem-header.php';
 
//Longpage Contents
include 'header.php';
include 'partials/hero.php';
include 'partials/text-hero.php';
include 'partials/problem-1.php'; 
include 'partials/problem-2.php';
include 'partials/problem-3.php'; 
include 'partials/cta.php';  
include 'footer.php';
?>

